### Node Express template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.


https://immense-woodland-87607.herokuapp.com/catalog/




heroku logs --tail
git push heroku master





## Schema
book
	title: string, required
	isbn string, required
	author:
		first_name
		family_name
		date_of_birth
		date_of_death
	genre
		name []
	bookInstance
		due_date
		status []
		imprint
		
## Frontend Features
- User can register/login/logout
- User can add/update/delete book
- User can search books by book name
- User can browser books according to category
## Admin Features
- Admin can log in/logout
- Admin dashboard shows the followings
- Total Categories Count
- Total Users Count
- Total Books Count
- Admin can add/edit/delete category
- Admin can delete users
- Admin can delete books	


