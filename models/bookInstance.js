const mongoose = require('mongoose')
const Schema = mongoose.Schema

const BookInstanceSchema = new Schema({ 
  book: {
    type: Schema.Types.ObjectId, 
    required: true,
    ref: 'Book'
  },
  imprint: {
    type: String,
    required: true    
  },
  status: {
    type: String,
    required: true,
    enum: ['Available', 'Maintenance', 'Loan', 'Reserved'],
    default: 'Maintenance' 
  },
  due_date: {
    type: Date,    
    default:Date.now    
  }
}, {timestamps: true})


// Virtual for Genre Url
BookInstanceSchema
.virtual('url')
.get(() => {
  return '/catalog/bookInstance/' + this._id
})

const BookInstance = mongoose.model('BookInstance', BookInstanceSchema)
module.exports = BookInstance
