const mongoose = require('mongoose')
const Schema = mongoose.Schema

const GenreSchema = new Schema({
  name: {
    type:String, 
    required: true,
    minlength: 3,
    maxlength: 100
  }
}, {timestamps:true})

// Virtual for Genre Url
GenreSchema
.virtual('url')
.get(() => {
  return '/catalog/genre/' + this._id
})

const Genre = mongoose.model('Genre', GenreSchema)
module.exports = Genre
