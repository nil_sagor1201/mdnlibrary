const mongoose = require('mongoose')
const Schema = mongoose.Schema

const BookSchema = new Schema({
  title: {
    type:String, 
    required: true    
  },
  author: {
    type: Schema.Types.ObjectId, 
    required: true,
    ref: 'Author'
  },
  summary: {
    type: String,
    required: true    
  },
  isbn: {
    type: String,
    required: true    
  },
  genre: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Genre'    
  }
}, {timestamps: true})


// Virtual for Genre Url
BookSchema
.virtual('url')
.get(() => {
  return '/catalog/book/' + this._id
})

const Book = mongoose.model('Book', BookSchema)
module.exports = Book
