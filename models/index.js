const mongoose = require('mongoose')
const dev_db_url = 'mongodb+srv://dbUser:dbUser12345678@cluster0.fgl8f.gcp.mongodb.net/local_library?retryWrites=true&w=majority'
const mongodB = process.env.MONGODB_URI || dev_db_url
mongoose.connect(mongodB, {useNewUrlParser: true, useUnifiedTopology: true})

const Genre = require('./genre')
const Author = require('./author')
const Book = require('./book')
const BookInstance = require('./bookInstance')

const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))

module.exports = {
  Genre,
  Author,
  Book,
  BookInstance
}