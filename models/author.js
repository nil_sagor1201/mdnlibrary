const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AuthorSchema = new Schema({
  first_name: {
    type:String, 
    required: true,
    minlength: 3,
    maxlength: 100
  },
  family_name: {
    type:String, 
    required: true,
    minlength: 3,
    maxlength: 100
  },
  date_of_birth: {
    type:Date    
  },
  date_of_death: {
    type:Date    
  }
}, {timestamps: true})

AuthorSchema
.virtual('name')
.get(() => {
  let fullname = ''
  if (this.first_name && this.family_name) {
    fullname = this.family_name + ', ' + this.first_name
  }
  if (!this.first_name || !this.family_name){
    fullname = ''
  }
  return fullname
})

AuthorSchema
  .virtual('lifespan')
  .get(() => {
  return (this.date_of_death.getYear() - this.date_of_birth.getYear()).toString()
})


// Virtual for Genre Url
AuthorSchema
  .virtual('url')
  .get(() => {
  return '/catalog/author/' + this._id
})

const Author = mongoose.model('Author', AuthorSchema)
module.exports = Author
