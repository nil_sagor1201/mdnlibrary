const express = require('express')
const router = express.Router()

const genre = require('../controllers/genreController')
const author = require('../controllers/authorController')
const book = require('../controllers/bookController')
const bookInstance = require('../controllers/bookInstanceController')

router.get('/genre/create', genre.genre_create_get)

router.post('/genre/create', genre.genre_create_post)

router.get('/genre/:id', genre.genre_detail)


// author routes
router.get('/author/create', author.author_create_get)

router.post('/author/create', author.author_create_post)

router.get('/authors', author.author_list)

// router.get('/author/:id', author.author_detail)

// book routes
router.get('/', book.index)
router.get('/book/create', book.book_create_get)

// router.post('/book/create', book.book_create_post)

// router.get('/book/:id', book.book_detail)



// bookInstance routes
router.get('/bookInstance/create', bookInstance.bookInstance_create_get)

// router.post('/book/create', book.book_create_post)

// router.get('/book/:id', book.book_detail)

module.exports = router