const validator = require('express-validator')
const mongoose = require('mongoose')
const { Genre, Book } = require('../models')



function genre_list(req, res) {
  res.send('Not Implemented: Genre List')
}

async function genre_detail(req, res, next) {
  try {
    const genreId = req.params.id
    const genre = await Genre.findById(genreId)
    const genre_books = await Book.find({'genre': req.params.id})

    if (genre == null){
      return next()
    }
    res.status(404).send('Not found')
  } finally {
    res.status(400).send("Invalid")
  }

  res.render('genre_detail', {title: 'Genre Detail', genre, genre_books })
}

function genre_create_get(req, res) {
  res.render('genre_form', {title: 'Create Genre'})
}

function genre_create_post(req, res, next) {
  validator.check('name', 'Genre name required').trim().isLength({min:3})
  validator.body('name').escape()
  const errors = validator.validationResult(req)

  const genre = new Genre({
    name: req.body.name
  })

  if (!errors.isEmpty()){
    res.render('genre_form', {title: 'Create Genre', genre: genre, errors: errors.array()})
    return
  } else {
    Genre.findOne({'name': req.body.name})
      .exec( (err, found_genre) => {
        if (err) { return next(err)}
        if (found_genre) {
          res.redirect(found_genre.url)
        } else {
          genre.save((err) => {
            if (err) { return next(err)}
            res.redirect(genre.url)
          })
        }
      })
  }

  // res.send('Not Implemented: Genre List')
}

function genre_delete_get(req, res) {
  res.send('Not Implemented: Genre List')
}

function genre_delete_post(req, res) {
  res.send('Not Implemented: Genre List')
}

function genre_update_get(req, res) {
  res.send('Not Implemented: Genre List')
}

function genre_update_post(req, res) {
  res.send('Not Implemented: Genre List')
}

module.exports = {
  genre_list,
  genre_detail,
  genre_create_get,
  genre_create_post,
  genre_delete_get,
  genre_delete_post,
  genre_update_get,
  genre_update_post
}