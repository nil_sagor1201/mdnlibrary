const bookInstanceController = require('./bookInstanceController');

module.exports = {
  genre = require('./genreController'),
  author = require('./authorController'),
  book = require('./bookController'),
  bookInstanceController = require('./bookInstanceController')
}