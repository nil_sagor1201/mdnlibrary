const {body, validationResult} = require('express-validator')
const {sanitizeBody} = require('express-validator')
const mongoose = require('mongoose')
const { Book, Genre, Author } = require('../models')


async function index(req, res, next) {  
    const genre_count = await Genre.countDocuments({})
    res.render('index', {title: 'Local Library Home', data: genre_count})
  
}

function book_list(req, res) {
  res.send('Not Implemented: book List')
}

async function book_detail(req, res, next) {
  try {
    const bookId = req.params.id
    const book = await book.findById(bookId)
    const book_books = await Book.find({'book': req.params.id})

    if (book == null){
      return next()
    }
    res.status(404).send('Not found')
  } finally {
    res.status(400).send("Invalid")
  }

  res.render('book_detail', {title: 'book Detail', book, book_books })
}

async function book_create_get(req, res, next) {
  const authors = await Author.find()
  const genres = await Genre.find()
  res.render('book_form', {title: 'Create book', authors, genres })
}

async function book_create_post(req, res, next) {
  body('first_name').isLength({min: 1}).trim().withMessage('First name must be specified')
    .isAlphanumeric().withMessage('First name has non-alphanumric'),
  body('family_name').isLength({min: 1}).trim().withMessage('family name must be specified')
    .isAlphanumeric().withMessage('family name has non-alphanumric')
  body('date_of_birth', "invalid date of birth").optional({checkFalsy: true}).isISO8601(),
  body('date_of_death', "invalid date of death").optional({checkFalsy: true}).isISO8601()



  // sanitizeBody('first_name').escape(),
  // sanitizeBody('family_name').escape(),
  // sanitizeBody('date_of_birth').escape(),
  // sanitizeBody('date_of_death').escape()

  const errors = validationResult(req)

  try {
    const book = new book({
      first_name: req.body.first_name,
      family_name: req.body.family_name,
      date_of_birth: req.body.date_of_birth,
      date_of_death: req.body.date_of_death
    })
  
    if (!errors.isEmpty()){
      res.render('book_form', {title: 'Create book', book: book, errors: errors.array()})
      return
    } else {
      await book.save((err) => {
        if (err) { return next(err)}
        res.redirect(book.url)
      })
    }
  } catch (err) {
    res.status(404).send("Invalid")
  }
}
  

  


function book_delete_get(req, res) {
  res.send('Not Implemented: book List')
}

function book_delete_post(req, res) {
  res.send('Not Implemented: book List')
}

function book_update_get(req, res) {
  res.send('Not Implemented: book List')
}

function book_update_post(req, res) {
  res.send('Not Implemented: book List')
}

module.exports = {
  index,
  book_list,
  book_detail,
  book_create_get,
  book_create_post,
  book_delete_get,
  book_delete_post,
  book_update_get,
  book_update_post
}