const {body, validationResult} = require('express-validator')
const {sanitizeBody} = require('express-validator')
const mongoose = require('mongoose')
const { BookInstance, Book, Genre, Author } = require('../models')


function bookInstance_list(req, res) {
  res.send('Not Implemented: bookInstance List')
}

async function bookInstance_detail(req, res, next) {
  try {
    const bookInstanceId = req.params.id
    const bookInstance = await bookInstance.findById(bookInstanceId)
    const bookInstance_bookInstances = await bookInstance.find({'bookInstance': req.params.id})

    if (bookInstance == null){
      return next()
    }
    res.status(404).send('Not found')
  } finally {
    res.status(400).send("Invalid")
  }

  res.render('bookInstance_detail', {title: 'bookInstance Detail', bookInstance, bookInstance_bookInstances })
}

async function bookInstance_create_get(req, res, next) {
  const books = await Book.find()  
  res.render('bookInstance_form', {title: 'Create bookInstance', book_list: books })
}

async function bookInstance_create_post(req, res, next) {
  body('first_name').isLength({min: 1}).trim().withMessage('First name must be specified')
    .isAlphanumeric().withMessage('First name has non-alphanumric'),
  body('family_name').isLength({min: 1}).trim().withMessage('family name must be specified')
    .isAlphanumeric().withMessage('family name has non-alphanumric')
  body('date_of_birth', "invalid date of birth").optional({checkFalsy: true}).isISO8601(),
  body('date_of_death', "invalid date of death").optional({checkFalsy: true}).isISO8601()



  // sanitizeBody('first_name').escape(),
  // sanitizeBody('family_name').escape(),
  // sanitizeBody('date_of_birth').escape(),
  // sanitizeBody('date_of_death').escape()

  const errors = validationResult(req)

  try {
    const bookInstance = new bookInstance({
      first_name: req.body.first_name,
      family_name: req.body.family_name,
      date_of_birth: req.body.date_of_birth,
      date_of_death: req.body.date_of_death
    })
  
    if (!errors.isEmpty()){
      res.render('bookInstance_form', {title: 'Create bookInstance', bookInstance: bookInstance, errors: errors.array()})
      return
    } else {
      await bookInstance.save((err) => {
        if (err) { return next(err)}
        res.redirect(bookInstance.url)
      })
    }
  } catch (err) {
    res.status(404).send("Invalid")
  }
}
  

  


function bookInstance_delete_get(req, res) {
  res.send('Not Implemented: bookInstance List')
}

function bookInstance_delete_post(req, res) {
  res.send('Not Implemented: bookInstance List')
}

function bookInstance_update_get(req, res) {
  res.send('Not Implemented: bookInstance List')
}

function bookInstance_update_post(req, res) {
  res.send('Not Implemented: bookInstance List')
}

module.exports = { 
  bookInstance_list,
  bookInstance_detail,
  bookInstance_create_get,
  bookInstance_create_post,
  bookInstance_delete_get,
  bookInstance_delete_post,
  bookInstance_update_get,
  bookInstance_update_post
}