const {body, validationResult} = require('express-validator')
const {sanitizeBody} = require('express-validator')
const mongoose = require('mongoose')
const { Author } = require('../models')



async function author_list(req, res, next) {
  const list_authors = await Author.find().populate('author').sort([['family_name', 'ascending']])
  // res.send('Not Implemented: author List')
  res.render('author_list', { title: 'Author List', author_list: list_authors })
}

async function author_detail(req, res, next) {
 
    // const authorId = req.params.id
    const author = await Author.findById({_id: req.params.id})
    // const author_books = await Book.find({'author': req.params.id})

    if (author == null){
      // return next()
      res.status(404).send('Not found')
    }
  
    res.render('author_detail', {title: 'author Detail', author, author_books })
  
  }
  // res.render('author_detail', {title: 'author Detail', author, author_books })


function author_create_get(req, res, next) {
  res.render('author_form', {title: 'Create author'})
}

async function author_create_post(req, res, next) {
  body('first_name').isLength({min: 1}).trim().withMessage('First name must be specified')
    .isAlphanumeric().withMessage('First name has non-alphanumric'),
  body('family_name').isLength({min: 1}).trim().withMessage('family name must be specified')
    .isAlphanumeric().withMessage('family name has non-alphanumric')
  body('date_of_birth', "invalid date of birth").optional({checkFalsy: true}).isISO8601(),
  body('date_of_death', "invalid date of death").optional({checkFalsy: true}).isISO8601()



  // sanitizeBody('first_name').escape(),
  // sanitizeBody('family_name').escape(),
  // sanitizeBody('date_of_birth').escape(),
  // sanitizeBody('date_of_death').escape()

  const errors = validationResult(req)
  
  
  
  const author = new Author({
    first_name: req.body.first_name,
    family_name: req.body.family_name,
    date_of_birth: req.body.date_of_birth,
    date_of_death: req.body.date_of_death
  })


  try {   
    if (!errors.isEmpty()){
      res.render('author_form', {title: 'Create author', author: author, errors: errors.array()})
      return
    } else {
      await author.save((err) => {
        if (err) { return next(err)}
        res.redirect(author.url)
      })
    }
  } catch (err) {
    res.status(404).send("Invalid")
  }
}
  

  


function author_delete_get(req, res) {
  res.send('Not Implemented: author List')
}

function author_delete_post(req, res) {
  res.send('Not Implemented: author List')
}

function author_update_get(req, res) {
  res.send('Not Implemented: author List')
}

function author_update_post(req, res) {
  res.send('Not Implemented: author List')
}

module.exports = {
  author_list,
  author_detail,
  author_create_get,
  author_create_post,
  author_delete_get,
  author_delete_post,
  author_update_get,
  author_update_post
}